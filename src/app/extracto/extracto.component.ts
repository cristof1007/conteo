import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService,Mesa } from '../rest.service'

@Component({
  selector: 'app-extracto',
  templateUrl: './extracto.component.html',
  styleUrls: ['./extracto.component.scss']
})
export class ExtractoComponent implements OnInit {

  constructor(public rest: RestService, private router:Router) { }

  ngOnInit(): void {
    console.log('iniciado');
    let mesa = new Mesa();
    mesa.codigoMesa = '1005476';
    this.rest.getMesa(mesa).subscribe((result) => {
      console.log(result.datoAdicional.tabla);
    }, (err) => {
      console.log(err);
    });
  }

}
