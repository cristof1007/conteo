import { Injectable } from '@angular/core';

import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  endpoint = 'https://computo.oep.org.bo/api/v1/resultados/mesa';
  constructor(private http: HttpClient) { 

  }

  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }

  getMesa(mesa: Mesa): Observable<any> {
    let headers = new HttpHeaders().set('access-control-allow-origin', '*');
    console.log(mesa);
    return this.http.post(this.endpoint, mesa, {headers: headers}).pipe(
      catchError(this.handleError)
    );
  }
}


export class Mesa
{
  codigoMesa:string;
}